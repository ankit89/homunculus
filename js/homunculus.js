(function(jQuery, undefined){
    var homunculus = {};

    homunculus.initialize = function(targetElement){
	// TODO how to do layers in p5.js
	// TODO how to add editable shapes in p5.js
	// draw the homunculus as a background layer
	// allowing the user to draw on a sketch on top of the homunculus

	addHomonculusLayer();
	addSketchLayer();
    }
    

    // this function serializes the sketch (i.e. the different layers)
    // and returns the serialized json object
    homunculus.exportAsJson = function(){
    }

    // this functions takes a json object (which represents the different layers of the sketch)
    // and adds layers based on the json
    homunculus.importJson = function(json){
    }

    //this function add the layer which contains the homunculus
    function addHomunculusLayer(){
    }

    //this function adds the layer which allows the user to draw shapes on top of the homunculus
    function addSketchLayer(){
    }

    //This function adds event handlers to enable user interaction.
    function addInteraction(){
	//TODO add code which allows user interactions
	// 1. adding a shape
	// 2. deleting a shape
	// 3. editing a shape (intensity, size _define-visual-encoding-schema_)
    }
    
    return homunculus;
})(jQuery, undefined);
