var painLevel;
var ER_SIZE = 30;
var timestamp = Math.floor(Date.now() / 1000);

//TODO this participant id can be retrieved from a login page
var participantId = 1;
//TODO this participant username should also be retrieved from the login page
var participantName = "test";

var sketchTransactions = [];

var activeTransaction = undefined;

var tools = {};

function registerTool(toolName, options) {
    tools[toolName] = options;
}

registerTools("pain", {
    onMousePressed: function () {
	if(activeTransaction) {
	    activeTransaction.path.push({x: mouseX, y: mouseY});
	} else {
	    activeTransaction = {tool: "pain", level: painLevel, path: [] };
	}
	line(mouseX, mouseY, pmouseX, pmouseY);
    },
    onMouseReleased: function () {
	if ( activeTransaction ) {
	    sketchTransactions.push(activeTransaction);
	}
    }
});

function setup() {
    createCanvas(640, 480);
    setpain(1);
}

function draw() {
    if(mouseIsPressed) {
	tools[currentTool].onMousePressed();
    }else{
	tools[currentTool].onMouseReleased();
    }
}

function saveCanvas() {
    var localStorageKey = participantId + "_" + participantName + "_" + timestamp;
    localStorage.setItem(localStorageKey, sketchTransactions);
}

function setpain(level) {
      
}

$(function () {
    //add event listener to set pain as the tool

    //read the pain level from the slider
})
